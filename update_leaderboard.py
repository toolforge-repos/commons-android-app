#!/usr/bin/env python3

import os
import cgi
import sys
import json
import yaml
import pymysql
import requests

# Print header
print('Content-type: application/json\n')


def jsonify(response):
    return json.dumps(response)


def get_res(status, user, message):
    response = {'status': status, 'user': user, 'message': message}
    return jsonify(response)


# Fetch params
if 'QUERY_STRING' in os.environ:
    QS = os.environ['QUERY_STRING']
    qs = cgi.parse_qs(QS)
    try:
        username = qs['user'][0].replace('_', ' ')
    except:
        print(get_res('400', None, 'Invalid Parameters'))
        sys.exit(0)

    # Load config
    __dir__ = os.path.dirname(__file__)
    config = yaml.safe_load(open(os.path.join(__dir__, 'config.yaml')))
    conn = pymysql.connect(db=config['DB_NAME'],
                           host=config['DB_HOST'],
                           user=config['DB_USER'],
                           password=config['DB_PASS'],
                           charset="utf8",
                           )
else:
    print(get_res('500', username, 'Unable to connect to database'))
    sys.exit(0)


##### PROGRAM ####


def get_nearby_data(username):
    url = 'https://tools.wmflabs.org/commons-android-app/tool-commons-android-app/nearby_by_user_and_duration.py?user={username}'.format(
        username=username)
    body = json.loads(requests.get(url).content)
    return body['data']


def get_used_data(username):
    url = 'https://tools.wmflabs.org/commons-android-app/tool-commons-android-app/used_images_by_user_and_duration.py?user={username}'.format(
        username=username)
    body = json.loads(requests.get(url).content)
    return body['data']


def get_upload_data(username):
    url = 'https://tools.wmflabs.org/commons-android-app/tool-commons-android-app/uploads_by_user_and_duration.py?user={username}'.format(
        username=username)
    body = json.loads(requests.get(url).content)
    return body['data']


def get_sql_user_data(username, weekly, yearly, all_time, _type):
    return """UPDATE leaderboard_{_type} as t1,
    (select user_id from leaderboard_user where username='{username}') as t2
    SET t1.weekly = '{weekly}', t1.yearly = '{yearly}', t1.all_time = '{all_time}'
    WHERE t1.user_id = t2.user_id;""".format(
        username=username, weekly=weekly, yearly=yearly, all_time=all_time, _type=_type)


def update_user_data(username, weekly, yearly, all_time, _type):
    cur = conn.cursor()
    with cur:
        sql = get_sql_user_data(username, weekly, yearly, all_time, _type)
        cur.execute(sql)
        conn.commit()


try:
    data_used = get_used_data(username)
    update_user_data(username, data_used['weekly'],
                  data_used['yearly'], data_used['all_time'], 'used')

    data_nearby = get_nearby_data(username)
    update_user_data(username, data_nearby['weekly'],
                  data_nearby['yearly'], data_nearby['all_time'], 'nearby')

    data_upload = get_upload_data(username)
    update_user_data(username, data_upload['weekly'],
                  data_upload['yearly'], data_upload['all_time'], 'upload')

    print(get_res('200', username, 'Data Updated'))

except:
    print(get_res('500', username, 'Internal Server Error'))
    sys.exit(0)
