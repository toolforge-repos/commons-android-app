#!/usr/bin/env python3

import os
import cgi
import sys
from wmflabs import db
import json
import yaml
import pymysql

# Print header
print('Content-type: application/json\n')


def jsonify(response):
    return json.dumps(response)


def get_res(status, user, exists):
    response = {'status': status, 'user': user,  'exists': exists}
    return jsonify(response)


def get_res_error(status, user, message):
    response = {'status': status, 'user': user, 'message': message}
    return jsonify(response)


# Fetch params
labs = False
if 'QUERY_STRING' in os.environ:
    QS = os.environ['QUERY_STRING']
    qs = cgi.parse_qs(QS)
    try:
        username = qs['user'][0].replace('_', ' ')
    except:
        print(get_res_error('400', username, 'Invalid Parameters'))
        sys.exit(0)
    if 'labs' in qs:
        labs = True
        # Load config
        __dir__ = os.path.dirname(__file__)
        config = yaml.safe_load(open(os.path.join(__dir__, 'config.yaml')))
        conn = pymysql.connect(db=qs['labs'][0],
                               host=config['DB_HOST'],
                               user=config['DB_USER'],
                               password=config['DB_PASS'],
                               charset="utf8",
                               )
    else:
        try:
            conn = db.connect(qs['db'][0])
        except:
            conn = db.connect('commonswiki')
else:
    print(get_res_error('400', username, 'Invalid Parameters'))
    sys.exit(0)


##### PROGRAM ####


def get_sql(username):
    return '''select count(actor_id) from actor where actor_name="{username}";'''.format(username=username)


def get_count(username):
    cur = conn.cursor()
    with cur:
        sql = get_sql(username)
        if labs:
            sql = sql.replace('_userindex', '')
        cur.execute(sql)
        data = cur.fetchall()
    return data[0][0]


try:
    data = get_count(username)
    if int(data) > 0:
        print(get_res('200', username, True))
    else:
        print(get_res('200', username, False))
except:
    print(get_res_error('500', username, 'Internal Server Error'))
    sys.exit(0)
