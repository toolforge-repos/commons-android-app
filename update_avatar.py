#!/usr/bin/env python3

import os
import cgi
import sys
import json
import yaml
import pymysql
import requests

# Print header
print('Content-type: application/json\n')


def jsonify(response):
    return json.dumps(response)


def get_res(status, user, message):
    response = {'status': status, 'user': user, 'message': message}
    return jsonify(response)


# Fetch params
if 'QUERY_STRING' in os.environ:
    QS = os.environ['QUERY_STRING']
    qs = cgi.parse_qs(QS)
    try:
        avatar = qs['avatar'][0]
        username = qs['user'][0].replace('_', ' ')
    except:
        print(get_res('400', None, 'Invalid Parameters'))
        sys.exit(0)

    # Load config
    __dir__ = os.path.dirname(__file__)
    config = yaml.safe_load(open(os.path.join(__dir__, 'config.yaml')))
    conn = pymysql.connect(db=config['DB_NAME'],
                           host=config['DB_HOST'],
                           user=config['DB_USER'],
                           password=config['DB_PASS'],
                           charset="utf8",
                           )
else:
    print(get_res('500', username, 'Unable to connect to database'))
    sys.exit(0)


##### PROGRAM ####


def get_sql(username, avatar):
    return """UPDATE leaderboard_user as t1, 
    (select user_id from leaderboard_user where username='{username}') as t2
    SET t1.user_avatar = '{avatar}' 
    WHERE t1.user_id = t2.user_id;""".format(
        username=username, avatar=avatar)


def update_avatar(username, avatar):
    if len(username) > 0 and len(avatar) > 0:
        cur = conn.cursor()
        with cur:
            sql = get_sql(username, avatar)
            cur.execute(sql)
            conn.commit()


try:
    update_avatar(username, avatar)
    print(get_res('200', username, 'Avatar Updated'))

except:
    print(get_res('500', username, 'Internal Server Error'))
    sys.exit(0)
