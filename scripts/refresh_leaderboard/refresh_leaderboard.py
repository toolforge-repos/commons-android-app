print("Job Started")
try:
    import requests
    import time

    r = requests.get("https://commons-android-app.toolforge.org/tool-commons-android-app/leaderboard.py?user=Madhurgupta10&duration=weekly&category=upload")

    users = r.json()
    print("Leaderboard List Fetched")

    for user in users['leaderboard_list']:
        print("Update process started for user {username}".format(username=user['username']))
        start = time.process_time()
        r = requests.get("https://commons-android-app.toolforge.org/tool-commons-android-app/update_leaderboard.py?user={username}".format(username=user['username']))
        print(time.process_time() - start)
        print("Update process completed for user {username}".format(username=user['username']))
    
    print("Job Completed")

except:
    print("Job Terminated with an Exception")